require('./bootstrap');
window.Vue = require('vue');

import Autocomplete from 'v-autocomplete'
import 'v-autocomplete/dist/v-autocomplete.css'
Vue.use(Autocomplete)

import Form from './services/form'
window.Form = Form;
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('app', require('./app.vue'));

const app = new Vue({
    el: '#app'
});
