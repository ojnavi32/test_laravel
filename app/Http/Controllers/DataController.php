<?php

namespace App\Http\Controllers;

use App\Data;
use App\Http\Requests\SearchValidateRequest;
use Illuminate\Http\Request;

class DataController extends Controller
{
    public function itemsList(  )
    {
    	$items = Data::all();
		return $this->success(compact('items'));
    }

    public function search( Request $request )
    {
    	$priceRange = $request['price'];
    	$result = Data::where('name', 'like', '%'.$request['name'].'%')
    		->whereBetween('price' ,[$priceRange[0], $priceRange[1]]);

    	if (!empty($request['bedrooms']))
    		$result->where('bedrooms', $request['bedrooms']);

    	if (!empty($request['bathrooms']))
    		$result->where('bathrooms', $request['bathrooms']);

    	if (!empty($request['storeys']))
    		$result->where('storeys', $request['storeys']);

    	if (!empty($request['garages']))
    		$result->where('garages', $request['garages']);

    	$results = $result->get();

    	if (count($results) === 0) {
			return $this->error(['message' => 'No results were found...']);
    	}

    	return $this->success(compact('results', 'priceRange'));
    }
}
