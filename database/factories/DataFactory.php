<?php

use Faker\Generator as Faker;

$factory->define(App\Data::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'price' => rand(111111, 999999),
        'bedrooms' => rand(1, 10),
        'bathrooms' => rand(1, 10),
        'storeys' => rand(1, 10),
        'garages' => rand(1, 10),
    ];
});
